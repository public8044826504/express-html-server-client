const express = require('express');
const router = express.Router();
const session = require('express-session');
const { body, validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const sanitizeHtml = require('sanitize-html');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const helmet = require('helmet');
const helmetCSP = require('helmet-csp');
const xss = require('xss-clean');
const hpp = require('hpp');
const axios = require('axios');
const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database('./database/mydatabase.db');

// Create a CSRF protection middleware
const csrf = require('csurf')({ cookie: true });

// Configure express-session middleware
const secretKey = crypto.randomBytes(32).toString('hex');
console.log('Secret Key:', secretKey);
router.use(
    session({
      secret: secretKey,
      resave: false,
      saveUninitialized: false,
    })
)

// Initialize Passport
router.use(passport.initialize());
router.use(passport.session());

// Configure Passport to use the Local Strategy for authentication
passport.use(new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    async (email, password, done) => {
      try {

        // Fetch the user from the database by email
        const user = await db.get('SELECT email FROM users WHERE email = ?', [email]);

        if (!user || !await bcrypt.compare(password, user.password)) {
          return done(null, false, { message: 'Invalid email or password' });
        }

        return done(null, user);
      } catch (error) {
        return done(error);
      }
    }
));

// Serialize and deserialize user sessions (for Passport)
passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  try {
    const user = await db.get('SELECT id FROM users WHERE id = ?', [id]);
    done(null, user);
  } catch (error) {
    done(error);
  }
});

// Helmet middleware for security headers
router.use(helmet());

// Helmet Content Security Policy (CSP) middleware
router.use(helmetCSP());

// XSS Clean middleware to sanitize user inputs
router.use(xss());

// HPP (HTTP Parameter Pollution) protection middleware
router.use(hpp());

// Define a route for the signup page (GET request)
router.get('/signup', csrf, (req, res) => {
  res.render('signup', { csrfToken: req.csrfToken() });
});

// Define a route to handle the signup form submission (POST request)
router.post('/signup', csrf, [
  body('username').notEmpty().trim().escape(),
  body('email').isEmail().normalizeEmail(),
  body('password').isLength({ min: 5 }).escape(),
], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  // Destructure user input from the request body
  const { username, email, password } = req.body;

  // Sanitize user inputs to remove any potentially harmful HTML
  const sanitizedUsername = sanitizeHtml(username);
  const sanitizedEmail = sanitizeHtml(email);

  // Check if the email already exist in the database
  await db.get('SELECT email FROM users WHERE email = ?', [sanitizedEmail], async (err, existingEmail) => {
    if (err) {
      console.error(err);
      return res.status(500).json({ message: 'Internal server error' });
    }

    // Check if the username already exist in the database
    await db.get('SELECT username FROM users WHERE username = ?', [sanitizedUsername], async (err, existingUsername) => {
      if (err) {
        console.error(err);
        return res.status(500).json({ message: 'Internal server error' });
      }

      // If the email already exists, return an error response
      if (existingEmail) {
        return res.status(400).json({ message: 'Email already exists' });
      }

      // If the username already exists, return an error response
      if (existingUsername) {
        return res.status(400).json({ message: 'Username already exists' });
      }

      try {

        // Hash the password before storing it in the database
        const hashedPassword = await bcrypt.hash(password, 10);

        // Insert a new user into the users table
        await db.run('INSERT INTO users (username, email, password) VALUES (?, ?, ?)', [
          sanitizedUsername,
          sanitizedEmail,
          hashedPassword,
        ], function (err) {
          if (err) {
            console.error(err);
            return res.status(500).json({ message: 'Internal server error' });
          }

          // Redirect to a success page (e.g., login) after user registration
          res.redirect('/login');
        });
      } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
      }
    });
  });
});

// Define a route for the login page (GET request)
router.get('/login', csrf, (req, res) => {
  res.render('login', { csrfToken: req.csrfToken() });
});

// Define a route to handle the login form submission (POST request)
router.post('/login', csrf, passport.authenticate('local', {
  successRedirect: '/dashboard', // Redirect to a dashboard on successful login
  failureRedirect: '/login', // Redirect back to login page on failure
  failureFlash: true, // Enable flash messages on failure
}, (req, res) => {
  req.session.user = req.user; // Create a session cookie upon successful login
  res.cookie('sessionID', req.sessionID, { httpOnly: true });
}));

// Define a route handler that renders the 'template.ejs' template
router.get('/template', (req, res) => {
  res.render('template');
});

// Define an Express route handler for the form submission
router.post('/createTable', (req, res) => {
  const { username, tableName, columns } = req.body;

  if (!username || !tableName || !columns || !Array.isArray(columns)) {
    return res.status(400).json({ error: 'Invalid request data' });
  }

  // Generate a table name based on the user's username and an index (for uniqueness)
  const tableIndex = Math.floor(Math.random() * 1000); // Temporary index method for testing
  const generatedTableName = `${username}_${tableIndex}`;

  // Construct the CREATE TABLE SQL statement
  const createTableSQL = `CREATE TABLE ${generatedTableName} (${columns.map(col => `${col.name} ${col.type}`).join(', ')})`;

  // Execute the CREATE TABLE statement
  db.run(createTableSQL, err => {
    if (err) {
      return res.status(500).json({ error: 'Error creating table' });
    }

    res.status(201).json({ message: 'Table created successfully', tableName: generatedTableName });
  });
});

// Define an Express route handler for sending table data
router.get('/data/:tableName', async (req, res) => {
  const { tableName } = req.params;

  if (!tableName) {
    return res.status(400).json({ error: 'Table name is required' });
  }

  // Construct the SQL SELECT statement to fetch table data
  const selectTableSQL = `SELECT * FROM ${tableName}`;

  // Step 1: Fetch table columns and their names
  await db.all(`PRAGMA table_info(${tableName})`, async (err, columns) => {
    if (err) {
      return res.status(500).json({ error: 'Error fetching table columns' });
    }

    // Format the columns as a JSON array
    const columnInfo = columns.map(column => ({
      name: column.name,
      type: column.type,
    }));

    // Step 2: Fetch rows and their slots
    await db.all(selectTableSQL, (err, rows) => {
      if (err) {
        return res.status(500).json({ error: 'Error fetching table rows' });
      }

      // Format the rows and slots as a JSON object
      const rowData = {};
      rows.forEach(row => {
        rowData[row.id] = {
          name: row.name,
          value: row.value,
        };
      });

      // Send the JSON response
      res.json({ columns: columnInfo, rows: rowData });
    });
  });
});

module.exports = router;

